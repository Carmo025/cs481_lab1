// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Dialog Box Examples`
  String get formPageAppBarTitle {
    return Intl.message(
      'Dialog Box Examples',
      name: 'formPageAppBarTitle',
      desc: '',
      args: [],
    );
  }

  /// `Alert Dialog`
  String get firstSuggestedName {
    return Intl.message(
      'Alert Dialog',
      name: 'firstSuggestedName',
      desc: '',
      args: [],
    );
  }

  /// `CAUTION!`
  String get secondSuggestedName {
    return Intl.message(
      'CAUTION!',
      name: 'secondSuggestedName',
      desc: '',
      args: [],
    );
  }

  /// `High Surf, if in doubt dont go out`
  String get thirdSuggestedName {
    return Intl.message(
      'High Surf, if in doubt dont go out',
      name: 'thirdSuggestedName',
      desc: '',
      args: [],
    );
  }

  /// `Dismiss`
  String get formPageWeightInputLabel {
    return Intl.message(
      'Dismiss',
      name: 'formPageWeightInputLabel',
      desc: '',
      args: [],
    );
  }

  /// `Custom Dialog`
  String get formPageWeightInputSuffix {
    return Intl.message(
      'Custom Dialog',
      name: 'formPageWeightInputSuffix',
      desc: '',
      args: [],
    );
  }

  /// `How may I help you?`
  String get formPageRadioListLabel {
    return Intl.message(
      'How may I help you?',
      name: 'formPageRadioListLabel',
      desc: '',
      args: [],
    );
  }

  /// `Ask`
  String get formPageActionButtonTitle {
    return Intl.message(
      'Ask',
      name: 'formPageActionButtonTitle',
      desc: '',
      args: [],
    );
  }

  /// `Full Screen Dialog`
  String get formPageCustomDrinkRadioTitle {
    return Intl.message(
      'Full Screen Dialog',
      name: 'formPageCustomDrinkRadioTitle',
      desc: '',
      args: [],
    );
  }

  /// `Exit`
  String get formPageCustomDrinkServingSizeInputLabel {
    return Intl.message(
      'Exit',
      name: 'formPageCustomDrinkServingSizeInputLabel',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
      Locale.fromSubtags(languageCode: 'es', countryCode: 'ES'),
      Locale.fromSubtags(languageCode: 'pt', countryCode: 'BR'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}
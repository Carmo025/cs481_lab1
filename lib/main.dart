//Name: Carlitos Carmona, Tahwab Noori and Damian Salazar
//Email's:
// carmo025@cougars.csusm.edu
// noori016@cougars.csusm.edu
// salaz094@cougars.csusm.edu

//Professor: Zampell
//Class: CS 481 T/TH 7:00pm-8:15pm
//Lab 1
//The app demonstrates the use of localization, Internationalization and
//Dialog boxes


import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';

import 'package:flutter_app/generated/l10n.dart';

import 'package:flutter_localizations/flutter_localizations.dart';


void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        // 1
        S.delegate,
        // 2
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,

      title: 'Material App',
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  const Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dialog Box Examples"),
      ),
      body: Column(
        children: <Widget>[

//Create alert dialog with flatbutton
          FlatButton(
            child: Text("Alert Dialog"),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context){
                    return AlertDialog(
                      title: Text("CAUTION!"),
                      content: Text("High Surf, if in doubt dont go out"),
                      actions: <Widget>[
                        Image.asset('images/wave.PNG',
                          width: 120,
                          height: 120,
                          fit: BoxFit.cover,
                        ),


                        FlatButton(
                        child: Text("Dismiss"),

                          onPressed: (){
                          Navigator.of(context).pop();
                          },
                        )
                      ],
                    );
                  }
              );
            },
          ),
//Create custom dialog with flatbutton
          FlatButton(
            child: Text("Custom Dialog"),
            onPressed: (){
              showDialog(
                context: context,
                builder: (BuildContext buildContext){
                  return Dialog(

                  shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)
                  ),
                    child: Container(
                      height: 200,
                      child: Column(
                        children: <Widget>[
                          //Import images into app
                          Image.asset('images/question.jpeg',
                            width: 100,
                            height: 100,
                            fit: BoxFit.contain,
                          ),
                          TextField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "How may I help you?"

                            ),
                          ),
                          RaisedButton(
                            color: Colors.blue,
                            child: Text("Ask"),
                            onPressed: (){
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      ),
                    ),
                  );
                }
              );
            },
          ),
//Create full screen dialog with flatbutton
        FlatButton(
          child: Text("Full Screen Dialog"),
          onPressed: (){
            showGeneralDialog(
              barrierDismissible: false,
              barrierColor: Colors.orange,
              transitionDuration: const Duration(milliseconds: 200),
              context: context,
              pageBuilder: (BuildContext context, Animation animation, Animation secondAnimation)
              {
                return Center(

                  child: Container(


                    //To restrict the pop up size and width of dialog i can do so
                    //by implementing " - " followed by the numeric size
                    //For example " width: MediaQuery.of(context).size.width - 125 "
                    //This allows the color from "barrierColor: Colors.orange," to
                    //be diaplyed
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    color: Colors.red,
                    child: RaisedButton(

                      child: Text("Exit"),
                      onPressed: (){
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                );
              }

            );

          },
          )
        ],
      ),
    );
  }//end of widget build
}
import 'package:flutter/material.dart';

import 'package:flutter/material.dart';

import 'dart:async';



//Call our generated code and load up strings that need to be
//translated
//Load method takes in local type, checks country code
class AppLocalizations {
  static Future<AppLocalizations> load(Locale locale) {
    final String name =
    locale.countryCode.isEmpty ? locale.languageCode : locale.toString();

    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      //return applocalizations class
      return AppLocalizations();
    });
  }

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  String get title {
    return Intl.message(
        'Dialog Box Examples',
        name: 'title',
        desc: 'Alert Dialog'
    );
  }

  String get buttonText {
    return Intl.message(
        'CAUTION!',
        name: 'buttonText',
        desc: 'High Surf, if in doubt dont go out'
    );
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();
  //Input the supported language codes
  //English, spanish and japanese

  List<Locale> get supportedLocales {

    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
      Locale.fromSubtags(languageCode: 'de'),
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'pt', countryCode: 'BR'),


    ];
  }
  @override
  bool isSupported(Locale locale) => isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for(var supportedLocale in supportedLocales) {
        if( supportedLocale.languageCode == locale.languageCode) {

        }
      }
    }
  }
}
